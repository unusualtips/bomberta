#include <ncurses.h>
#include <unistd.h>
#include <string.h>

#include "Funcs.h"

int main(int argc, char *argv[]){
	initscr();
	noecho();
	curs_set(FALSE);

	int stopGame = 0;	// Флаг остановки приложения

	inputEvent clientInput;
	gameData clinetGameData;
	/*
	 * Меню
	 */

	/*
	 * Коннект по сети к серверу
	 */

	/*
	 * Загрузка карты
	 */

	// Главный цикл приложения
	while(!stopGame){
		clear();		// Очистка экрана

		/*
		 * Отлов эвентов
		 */

		/*
		 * Получаем данные из сети
		 */

		/*
		 * Обновление всех объектов
		 */

		/*
		 * Отправили данные в сеть
		 */

		/*
		 * Отрисовка объектов
		 */

		refresh();		// Обновление экрана

		sleep(1);		// Временная остановка выполнения
	}

	endwin();
	return 0;
}
