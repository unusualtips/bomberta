#ifndef FUNCS_H_
#define FUNCS_H_

typedef struct{

}inputEvent;

typedef struct{

}gameData;

void menu(char* __ipaddress, int* __port);

/*
 * 0 	- Ошибка
 * > 0 	- Успех, id игрока
 */
int connect(char* __ipaddress, int __port);

/*
 * 0 	- Ошибка
 * 1 	- Успех
 */
int loadMap();

void input(inputEvent* __input);

/*
 * 0 	- Ошибка
 * 1 	- Успех
 */
int getGameData(gameData* __gameData);

void update(inputEvent* __input, gameData* __gameData);

/*
 * 0 	- Ошибка
 * 1 	- Успех
 */
int send(gameData* __gameData, int __playerId);

void draw(gameData* __gameData);

#endif /* FUNCS_H_ */
